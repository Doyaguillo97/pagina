<?php
    require_once 'back/funcionesEdit.php';
    require_once 'back/login.php';
    if( $_SERVER['REQUEST_METHOD']=='GET') {
        session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <title> TecnoComponentes S.L </title>
    <link rel="shortcut icon" type="image/x-icon" href="imagenes/favicon.ico">
    <link rel="stylesheet" type="text/css" href="Code/CSS/productos.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/Estilo.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/Log-car.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="header">
        <div class="logo">
            <a href="index.php"> <img src="imagenes/logo.png" href="login" height="66px"></a>
        </div>

        <div class="top-menu">
            <nav>
                <ul class="menu-bar">
                    <li><a href="index.php"> Catálogo </a></li>
                    <li> <a href="quienessomos.php"> Quienes somos </a></li>
                    <li> <a href="filosofia.php"> Filosofia comercial </a></li>
                    <li> <a href="tiendas.php"> Tiendas Físicas </a></li>
                </ul>
            </nav>
        </div>

        <div class="log-car">
        <?php
            if(!isset($_SESSION['app1263467367346_islogged'])){
                print("<button onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;\">Login</button>");
                
            }elseif($_SESSION['app1263467367346_islogged']){
                print "<button onclick=\"location.href='/back/logout.php'\" style=\"width:auto;\">Logout</button>";
            }else{
                print("<button onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;\">Login</button>");
            }    
        ?>
            
            <div id="id01" class="modal">

            <form method="POST" class="modal-content animate">
                    <div class="imgcontainer">
                        <span onclick="document.getElementById('id01').style.display='none'" class="close"
                            title="Close Modal">&times;</span>
                        <img src="imagenes/avatar.png" alt="aqui" class="avatar">
                    </div>


                    <div class="container">
                        
                        
                            <label for="uname"><b>Usuario</b></label>
                            <input type="text" placeholder="Nombre de usuario" name="username" required>
                            <!--Nombre de usuario -->

                            <label for="psw"><b>Contraseña</b></label>
                            <input type="password" placeholder="•••••••••••" name="password" required>
                            <!--Contraseña mandar hasheada -->

                            <button type="submit">Login</button>
                        
                    </div>
                    
                    <!--Fin de login-->
                    <div class="container" style="background-color:#f1f1f1">
                        <button type="button" onclick="document.getElementById('id01').style.display='none'"
                            class="cancelbtn">Cancelar</button>
                        <span class="psw">¿No esta<a href="registro.php"> registrado</a>?</span>
                    </div>
                </form>
            </div>
            
			 <button onclick="location.href='carrito.php'"class=carrito style="width:auto;" >Carrito</button>
      
        </div>
        <div class="sombreado">

        </div>
    </div>
    
    
    
    <div class="cuerpo">
        <div class="producto">
            <div class="imagen">
                <img src="imagenes/portatil1.png" alt="Xiaomi Mi Air 13.3" width="300">
            </div>
            <div class="descripcion">
                <header>Xiaomi Mi Air 13.3"</header>
                <p><b>Precio: </b>899€</p>
                <p><b>Codigo de referencia:</b> X4578HJK9SS</p>
                <p><b>Descripcion: </b>El Xiaomi Mi Notebook Aires ligero, es compacto, y podrás llevarlo fácilmente a cualquier lugar sin darte cuenta de que lo llevas encima. Fabricado en metal de gran calidad y con unos acabados de primera, el portátil de Xiaomi hace presume de una extrema delgadez de tan solo 14.8 milímetros y 1,3 Kg de peso. En tan reducidas dimensiones disfrutamos de una pantalla de 13,3 pulgadas con resolución Full HD y cámara HD sin apenas marcos.</p>
                <form>
                    <p class="clasificacion" style="padding-top: 35px">
                        <input id="radio1" type="radio" name="estrellas" value="5">
                        <!--
    --><label for="radio1">★</label>
                        <!--
    --><input id="radio2" type="radio" name="estrellas" value="4">
                        <!--
    --><label for="radio2">★</label>
                        <!--
    --><input id="radio3" type="radio" name="estrellas" value="3">
                        <!--
    --><label for="radio3">★</label>
                        <!--
    --><input id="radio4" type="radio" name="estrellas" value="2">
                        <!--
    --><label for="radio4">★</label>
                        <!--
    --><input id="radio5" type="radio" name="estrellas" value="1">
                        <!--
    --><label for="radio5">★</label>
                    </p>
                </form>
                <p class="stock"><b>EN STOCK</b></p>
                <button class=carrito style="width:auto;">Añadir al carrito</button>
            </div>
            <div class="especificaciones">
                <ul>
                    <li><b>Procesador</b> Intel Core Kaby Lake i5-8250U (6M Cache, up to 3.40 GHz)</li>
                    <li><b>Memoria RAM</b> 8GB DDR4 2400Mhz</li>
                    <li><b>Almacenamiento</b> 256GB SSD + Slot de expasión SSD de hasta 1TB</li>
                    <li><b>Display</b> 13,3" (293,76 mm x 165,24 mm) Full HD de 1920x1080 a 16: 9, 166 PPI, 72% NTSC, 800: 1 y 300nit</li>
                    <li><b>Controlador gráfico</b> NVIDIA GeForce MX150 2GB GDDR5 VRAM</li>
                    <li><b>Conectividad</b>
                        <ul class="lista">
                            <li>Wireless-AC de Intel. Admite 2,4 GHz, banda dual de 5 GHz con MU-MIMO</li>
                            <li>Bluetooth 4.1</li>
                        </ul>
                    </li>
                    <li><b>Camara de portatil</b> Sí</li>
                    <li><b>Microfono</b> Sí</li>
                    <li><b>Audio</b> Tecnología Dolby Audio ™ Premium Surround</li>
                    <li><b>Batería</b> 4 celdas 40Wh</li>
                    <li><b>Conexiones</b>
                        <ul class="lista">
                            <li>USB-C</li>
                            <li>USB 3.0 x 2</li>
                            <li>HDMI</li>
                            <li>Jack 3.5</li>
                            <li>Lector de huellas</li>
                        </ul>
                    </li>
                    <li><b>Teclado</b> Teclado retroiluminado en español, distancia de recorrido de la tecla de 1,3 mm, que proporciona una mejor experiencia de escritura.</li>
                    <li><b>Sistema operativo</b> Windows 10 Home (64 bit)</li>
                    <li><b>Dimensiones</b> Diseño ultra delgado de 14.8mm</li>
                    <li><b>Peso</b> 1.3kg</li>
                    <li><b>Color</b> Gris Oscuro</li>
                    <li><b>Nota informativa:</b> Todos nuestros productos son distribuídos por canal oficial español, por lo que todos los teclados incluyen Ñ y poseen garantía oficial española.</li>

                </ul>
            </div>
        </div>



        <div class="bottom">
            <div class="footer-nav">
                <div class="row">
                    <ul>
                        <li>Contacto</li>
                        <li><a href="#">Teléfono</a> </li>
                        <li><a href="#">Email</a> </li>
                        <li><a href="#">Fax</a> </li>

                    </ul>
                </div>

                <div class="row">
                    <ul>
                        <li>Privacidad</li>
                        <li><a href="#">Politica de privacidad</a> </li>
                        <li><a href="#">Uso de cookies</a> </li>
                    </ul>
                </div>

                <div class="row">
                    <ul>
                        <li>Tiendas Fisicas</li>
                        <li><a href="#">Valladolid</a></li>
                        <li><a href="#">Palencia</a></li>
                        <li><a href="#">Medina del Campo</a></li>
                        <li><a href="#">Soria</a></li>
                    </ul>
                </div>

                <div class="row">
                    <ul>
                        <li>Empresa</li>
                        <li><a href="#">Acerca de TecnoComponentes </a> </li>
                        <li><a href="#">Comunidad</a> </li>
                        <li><a href="#">Empleo</a> </li>
                        <li><a href="#">Prensa</a> </li>

                    </ul>
                </div>
            </div>


        </div>
    </div>



<script type="text/javascript" src="Code/JS/login.js"></script>
</body>

</html>

<?php
                        }
                        else if( $_SERVER['REQUEST_METHOD']=='POST') {
                            
                            $result = login_check($_POST['username'], $_POST['password']);
                            //print_r($_POST); exit;
                            if( $result ) {
                                // Creo una sesion nueva
                                session_start();
                                $_SESSION['app1263467367346_islogged'] = true;
                                $_SESSION['username'] = $_POST['username'];
                                $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                                $_SESSION['type'] = login_privileges($result,$_POST['username']);
                                
                                //print_r($_SESSION); exit;
                                $perfil = "Location: perfil".$_SESSION['type'].".php";
                                header($perfil);//redireccionar a la pagina del usuario
                                exit;
                            }
                            else {
                                header('Location: error.php');// redireccionar a pagina de error
                                exit;
                            }
                        }
?>