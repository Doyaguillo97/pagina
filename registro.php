<?php
    require_once 'back/funcionesEdit.php';
    if( $_SERVER['REQUEST_METHOD']=='GET') {
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <title> TecnoComponentes S.L </title>
    <link rel="stylesheet" type="text/css" href="Code/CSS/registro.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

    <form id="regForm" method="POST">

        <h1>Formulario de Registro:</h1>

        <!-- las partes del formulario que se van a ir mostrando -->
        <div class="tab">
            <p><input  type="text" id="nombre" name="nombre" placeholder="Nombre" oninput="this.className = ''"></p>
            <p><input type="text"  name="primer_apellido" placeholder="Primer Apellido" oninput="this.className = ''"></p>
            <p><input type="text"  name="segundo_apellido" placeholder="Segundo Apellido" oninput="this.className = ''"></p>
        </div>

        <div class="tab">Informacion de Contacto:
            <p><input type="text" id="e_mail" name="e_mail" placeholder="E-mail" oninput="this.className = ''"></p>
            <p><input type="text" id="telefono" name="telefono" placeholder="Teléfono" oninput="this.className = ''"></p>
            <p><input type="text" id="direccion" name="direccion" placeholder="Dirección" oninput="this.className = ''"></p>
        </div>

        <div class="tab">Fecha de Nacimiento:
            <p><input type="text" id="day" name="day" placeholder="dd" oninput="this.className = ''"></p>
            <p><input type="text" id="month" name="month" placeholder="mm" oninput="this.className = ''"></p>
            <p><input type="text" id="year" name="year" placeholder="yyyy" oninput="this.className = ''"></p>
        </div>

        <div class="tab">Nombre de Usuario y Contraseña:
            <p><input type="text" id="username" name="username" placeholder="Username..." oninput="this.className = ''"></p>
            <p><input id="pass" name="pass" placeholder="Password..." type="password" oninput="this.className = ''"></p>
        </div>

        <div style="overflow:auto;">
            <div style="float:right;">
                <button type="button" id="nextBtn" onclick="nextPrev(1)">Siguiente</button> <!-- Capturar boton terminar???-->
            </div>
            <div style="float:left">
                <button type="button" id="prevBtn" onclick="nextPrev(-1)">Anterior</button>
            </div>
        </div>

        <!-- Circulos de abajo-->
        <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
        </div>

    </form>
    <script type="text/javascript" src="Code/JS/registro.js"></script>
</body>

</html>

<?php
    }
    else if( $_SERVER['REQUEST_METHOD']=='POST') {
        //print_r($_POST); exit;
        // Añado al usuario
        $u = usuarios_add($_POST['nombre'], $_POST['primer_apellido'], $_POST['segundo_apellido'],
        $_POST['e_mail'], $_POST['telefono'], $_POST['direccion'], $_POST['day'], $_POST['month'],
        $_POST['year'], $_POST['username'], md5($_POST['pass']));
        if( $u ) {
            header('Location: index.php');
            exit;
        }
        else {
            // Mostrar error o redirigir a error
            echo "Error: Falló la operación";
        }
    }
?>