<?php
/**
 * Obtiene todos los usuarios
 * @return array
 */
function usuarios_get_all()
{
    $usuarios = [];

    $data = json_decode(file_get_contents('../Usuarios/usuarios.json'), true);
    foreach ($data as &$u) {
        $usuarios[$u['username']] = $u;
    }

    return $usuarios;
}

/**
 * Comprueba si un usuario existe
 *
 * @param integer $uid  Identificador de usuario
 *
 * @return array|null
 */
function usuarios_existe($uid)
{
    $usuarios = usuarios_get_all();

    if (is_numeric($uid) && isset($usuarios[$uid])) {
        return $usuarios[$uid];
    }
    return null;
}

/**
 * Añade un usuario nuevo
 *
 * @param string $nombre                Nombre del usuario.
 * @param string $primer_apellido       Primer apellido.
 * @param string $segundo_apellido      Segundo apellido .
 * @param string $e_mail                E-mail usuario.
 * @param string $telefono              Telefono usuario.
 * @param string $direccion             Direccion usuario.
 * @param int $day                      Dia de nacimiento usuario.
 * @param int $month                    Mes de nacimiento de usuario.
 * @param int $year                     Año de nacimiento de usuario.
 * @param string $username              Nombre de usuario.
 * @param string $pass                  Contraseña del usuario.
 * @param string $type                  Tipo de usuario (admin, employed, user) por defecto usuario.
 * @param string $metodo                Metodo de pago del usuario. Por defecto se define como que no tiene
 *
 * @return array
 */
function usuarios_add($nombre, $primer_apellido, $segundo_apellido, $e_mail, $telefono, $direccion, $day, $month,
    $year, $username, $pass, $type = "user", $metodo = "predeterminado", $saldo = "0.0", $dni=null) {

    $usuarios = usuarios_get_all();

    //$id = max( array_keys($usuarios));

    $newusu = [
        'id' => $id + 1,
        'Nombre' => $nombre,
        'Primer Apellido' => $primer_apellido,
        'Segundo Apellido' => $segundo_apellido,
        'E-mail' => $e_mail,
        'Telefono' => $telefono,
        'Direccion' => $direccion,
        'dd' => $day,
        'mm' => $month,
        'yyyy' => $year,
        'username' => $username,
        'password' => $pass, //Puede pasarse directamente hasheada para mayor seguridad. Requiere hasear el string antes del envio.
        'type' => $type,
        'metodo' => $metodo,
        'saldo' => $saldo,
        'dni' => $dni
               
    ];

    $usuarios[$newusu['username']] = $newusu;
    file_put_contents('../Usuarios/usuarios.json', json_encode($usuarios));
    return $newusu;
}

/**
 * Edita un usuario
 *
 * 
 * 
 * @param string $username              Nombre de usuario.
 * @param string $POST datos enviados mediante post.
 * @return array
 */
function usuarios_edit($user, $POST)
{
    $usuarios = usuarios_get_all();
    //añadir el resto de elementos del array de un usuario y validar si estan llenos.
    if (isset($usuarios[$user])) {
        
        if($POST['saldo'] != null){
            $saldo = $POST['saldo'];

        }else{
            $saldo = 0;
        }
        
        if ($POST['direccion'] != null){
            $direccion = $POST['direccion'];
        }else{
            $direccion = $usuarios[$user]['Direccion'];
        }      

        if ($POST['pass'] != null) {
            $pass = md5($POST['password']);
        } else {
            $pass = $usuarios[$user]['password'];
        }
        
        if ($POST['nombre'] != null) {
            $nombre = $POST['nombre'];
        } else {
            $nombre = $usuarios[$user]['Nombre'];
        }

        if ($POST['primer_apellido'] != null) {
            $primer_apellido = $POST['primer_apellido'];
        } else {
            $primer_apellido = $usuarios[$user]['Primer Apellido'];
        }

        if ($POST['segundo_apellido'] != null) {
            $segundo_apellido = $POST['segundo_apellido'];
        } else {
            $segundo_apellido = $usuarios[$user]['Segundo Apellido'];
        }
        if ($POST['dni'] != null) {
            $dni = $POST['dni'];
        }

        $usuarios[$user]['Nombre'] = $nombre;
        $usuarios[$user]['Primer Apellido'] = $primer_apellido;
        $usuarios[$user]['Segundo Apellido'] = $segundo_apellido;
        $usuarios[$user]['password'] = $pass;
        $usuarios[$user]['Direccion'] = $direccion;
        $usuarios[$user]['saldo'] = $usuarios[$user]['saldo'] + $saldo;

    
        file_put_contents('../Usuarios/usuarios.json', json_encode($usuarios));
        return $usuarios[$user];
    }
    return null;
}

/**
 * Añade modifica el metodo de pago de un usuario
 * 
 * @param string $username              Nombre de usuario.
 * @param string $POST datos enviados mediante post.
 *
 *  @return array
 * 
 */

function añadir_Tarjeta($user, $POST)
{
    $usuarios = usuarios_get_all();
    //añadir el resto de elementos del array de un usuario y validar si estan llenos.
    if (isset($usuarios[$user])) {
        
        if ($POST['dni'] != null) {
            $tarjeta = $POST;
        }
        $usuarios[$user]['metodo'] = $tarjeta;
        file_put_contents('../Usuarios/usuarios.json', json_encode($usuarios));
        return $usuarios[$user];
    }
    return null;
}

/**
 * Edita un usuario
 *
 * @param integer $id           Id de usuario
 *
 * @return boolean
 */
function usuarios_delete($id)
{
    $usuarios = usuarios_get_all();

    if (isset($usuarios[$id])) {
        // Lo borramos
        unset($usuarios[$id]);
        file_put_contents('../Usuarios/usuarios.json', json_encode($usuarios));
        return true;
    }
    return false;
}
