<?php
/**
 * Obtiene todos los login
 * 
 * @return array
 */
function login_get_all() 
{    
    $usuarios = [];

    $data = json_decode(file_get_contents('../Usuarios/usuarios.json'), true);
    foreach($data as &$u) {
        $usuarios[$u['username']] = $u;
    }

    return $usuarios;
}

/**
 * Comprueba si un login existe
 * 
 * @param string $username  Username
 * @param string $password  Password
 * @return array|null
 */
function login_check($username, $password) 
{    
    $logins = login_get_all();

    if( isset($logins[$username]) ) {
        if( $logins[$username]['password']==md5($password)) {
            return true;
        }
    }
    return false;
}

function user_data($username){
    $logins = login_get_all();
    return $logins[$username];
}



function login_privileges($login,$username){//se crea una sesion nueva en funcioin del tipo de usuario
    if($login){
        $users = login_get_all();
        return $users[$username]['type'];
    }else{
        return "invitado";
    }
}
?>