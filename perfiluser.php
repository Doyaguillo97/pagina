<?php
    require_once 'back/funcionesEdit.php';
    require_once 'back/login.php';

    session_start();
    $usuario = $_SESSION['username']; // obtenemos el usuario de la sesion actual
    
    $user = user_data($usuario);// obtenemos un array con los datos del usuario

    if( $_SERVER['REQUEST_METHOD']=='GET') {
?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title> TecnoComponentes S.L </title>
    <link rel="shortcut icon" type="image/x-icon" href="imagenes/favicon.ico">
    <link rel="stylesheet" type="text/css" href="Code/CSS/productos.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/Estilo.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/Log-car.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/tarjeta.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>



<body>
    <div class="header">
        <div class="logo">
            <a href="index.php"> <img src="imagenes/logo.png" href="login" height="66px"></a>
        </div>

        <div class="top-menu">
            <nav>
                <ul class="menu-bar">
                    <li><a href="index.php"> Catálogo </a></li>
                    <li> <a href="quienessomos.php"> Quienes somos </a></li>
                    <li> <a href="filosofia.php"> Filosofia comercial </a></li>
                    <li> <a href="tiendas.php"> Tiendas Físicas </a></li>
                </ul>
            </nav>
        </div>

        <div class="log-car">
            <button onclick="location.href='/back/logout.php'" style="width:auto;">Logout</button>
            <!--Poner funcion unloggin -->

            <button onclick="location.href='carrito.php'" class=carrito style="width:auto;">Carrito</button>
        </div>
        <div class="sombreado">

        </div>
    </div>



    <div class="cuerpo">
        <div class="producto">
            <div class="imagen">
                <img src="imagenes/portatil1.png" alt="Xiaomi Mi Air 13.3" width="300">
            </div>
            <div class="descripcion" style=" text-align: center">
                <header>"Bienvenido <?php  echo($user['username'])?> "</header>
                <p><button onclick="location.href='tarjeta.php'" class=carrito style="width:auto;">Cambiar metodo de
                        pago</button></p>

                <div>
                    <button onclick="document.getElementById('añadirSaldo').style.display='block'"
                        style="width:auto;">Añadir saldo</button>

                    <div id="añadirSaldo" class="modal">
                        <div class="checkout">
                            <div class="credit-card-box">
                                <div class="flip">
                                    <div class="front">
                                        <div class="chip"></div>
                                        <div class="logo">
                                            <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834"
                                                style="enable-background:new 0 0 47.834 47.834;">
                                                <g>
                                                    <g>
                                                        <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                                                                c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                                                                c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                                                                M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                                                                c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                                                                c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                                                                l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                                                                C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                                                                C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                                                                c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                                                                h-3.888L19.153,16.8z" />
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="number"></div>
                                        <div class="card-holder">
                                            <label>Propietario</label>
                                            <div></div>
                                        </div>
                                        <div class="card-expiration-date">
                                            <label>Fecha de caducidad</label>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="back">
                                        <div class="strip"></div>
                                        <div class="logo">
                                            <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834"
                                                style="enable-background:new 0 0 47.834 47.834;">
                                                <g>
                                                    <g>
                                                        <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                                                                c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                                                                c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                                                                M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                                                                c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                                                                c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                                                                l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                                                                C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                                                                C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                                                                c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                                                                h-3.888L19.153,16.8z" />
                                                    </g>
                                                </g>
                                            </svg>

                                        </div>
                                        <div class="ccv">
                                            <label>CCV</label>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form class="form" autocomplete="off" method="POST">
                                <div class="imgcontainer" style="padding: 5% 5%">
                                    <!--Cruz de cierre -->
                                    <span onclick="document.getElementById('añadirSaldo').style.display='none'"
                                        class="close" title="Close Modal">&times;</span>
                                </div>
                                <fieldset>
                                    <label for="card-number">Número de tarjeta</label>
                                    <input type="num" id="card-number" name="card-number" class="input-cart-number"
                                        maxlength="4" />
                                    <input type="num" id="card-number-1" name="card-number-1" class="input-cart-number"
                                        maxlength="4" />
                                    <input type="num" id="card-number-2" name="card-number-2" class="input-cart-number"
                                        maxlength="4" />
                                    <input type="num" id="card-number-3" name="card-number-3" class="input-cart-number"
                                        maxlength="4" />
                                </fieldset>
                                <fieldset>
                                    <label for="card-holder">Prpietario</label>
                                    <input type="text" id="card-holder" name="property" />
                                </fieldset>
                                <fieldset class="fieldset-expiration">
                                    <label for="card-expiration-month">Expiration date</label>
                                    <div class="select">
                                        <select id="card-expiration-month" name="card-expiration-month">
                                            <option></option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                        </select>
                                    </div>
                                    <div class="select">
                                        <select id="card-expiration-year" name="card-expiration-year">
                                            <option></option>
                                            <option>2016</option>
                                            <option>2017</option>
                                            <option>2018</option>
                                            <option>2019</option>
                                            <option>2020</option>
                                            <option>2021</option>
                                            <option>2022</option>
                                            <option>2023</option>
                                            <option>2024</option>
                                            <option>2025</option>
                                        </select>
                                    </div>
                                </fieldset>
                                <fieldset class="fieldset-ccv">
                                    <label for="card-ccv">CCV</label>
                                    <input type="text" id="card-ccv" name="card-ccv" maxlength="3" />
                                </fieldset>

                                <fieldset class="fieldset-ccv">
                                    <label for="saldo">Cantidad</label>
                                    <input type="num" id="nuevoSaldo" name="saldo" />
                                </fieldset>
                                <button class="btn"><i class="fa fa-lock"></i> submit</button>
                            </form>
                        </div>
                    </div>
                </div>


                <div>
                    <button onclick="document.getElementById('cambioDireccion').style.display='block'"
                        style="width:auto;">Cambiar Direccion</button>

                    <div id="cambioDireccion" class="modal">
                        <form method="POST" class="modal-content animate">
                            <div class="imgcontainer" style="padding: 5% 5%">
                                <!--Cruz de cierre -->
                                <span onclick="document.getElementById('cambioDireccion').style.display='none'"
                                    class="close" title="Close Modal">&times;</span>
                            </div>
                            <div class="container">
                                <!--Password -->
                                <label><b>Nueva direccion</b></label>
                                <input type="text" placeholder="Nueva direecion" id="nuevo" name="direccion">

                                <!-- Envio y cancelación-->
                                <div class="container"
                                    style="background-color:#f1f1f1; margin: 0px; text-align:center;">
                                    <button type="button" style="padding: 10px; margin:5px; width: 40%; float:left "
                                        onclick="document.getElementById('cambioDireccion').style.display='none'"
                                        class="cancelbtn">Cancelar</button>

                                    <button type="submit" style="padding:10px; margin:5px; width: 40%; float:rigth">
                                        Aceptar </button>
                                </div>
                            </div>
                            <!--Fin de formulario-->
                        </form>
                    </div>
                </div>


                <div>
                    <button onclick="document.getElementById('cambioClave').style.display='block'"
                        style="width:auto;">Cambiar contraseña</button>

                    <div id="cambioClave" class="modal">
                        <form method="POST" class="modal-content animate">
                            <div class="imgcontainer" style="padding: 5% 5%">
                                <!--Cruz de cierre -->
                                <span onclick="document.getElementById('cambioClave').style.display='none'"
                                    class="close" title="Close Modal">&times;</span>
                            </div>
                            <div class="container">
                                <!--Password -->
                                <label><b>Nueva contraseña</b></label>
                                <input type="text" placeholder="Nueva contraseña" id="nuevo" name="password">

                                <!-- Envio y cancelación-->
                                <div class="container"
                                    style="background-color:#f1f1f1; margin: 0px; text-align:center;">
                                    <button type="button" style="padding: 10px; margin:5px; width: 40%; float:left "
                                        onclick="document.getElementById('cambioClave').style.display='none'"
                                        class="cancelbtn">Cancelar</button>

                                    <button type="submit" style="padding:10px; margin:5px; width: 40%; float:rigth">
                                        Aceptar </button>
                                </div>
                            </div>
                            <!--Fin de formulario-->
                        </form>
                    </div>
                </div>


                <div>
                    <button onclick="document.getElementById('datos').style.display='block'" style="width:auto;">Cambiar
                        datos de usuario</button>

                    <div id="datos" class="modal">
                        <form method="POST" class="modal-content animate">
                            <div class="imgcontainer" style="padding: 5% 5%">
                                <!--Cruz de cierre -->
                                <span onclick="document.getElementById('datos').style.display='none'" class="close"
                                    title="Close Modal">&times;</span>
                            </div>
                            <div class="container">
                                <!--Nombre del usuario -->
                                <label><b>Nombre</b></label>
                                <input type="text" placeholder="Nombre del usuario" name="nombre">
                                <!--Primer Apellido-->
                                <label><b>Primer Apellido</b></label>
                                <input type="text" placeholder="Primer Apellido" name="primer_apellido">
                                <!--Segundo Apellido-->
                                <label><b>Segundo Apellido</b></label>
                                <input type="text" placeholder="Segundo Apellido" name="segundo_apellido">
                                <!-- Envio y cancelación-->
                                <div class="container"
                                    style="background-color:#f1f1f1; margin: 0px; text-align:center;">
                                    <button type="button" style="padding: 10px; margin:5px; width: 40%; float:left "
                                        onclick="document.getElementById('datos').style.display='none'"
                                        class="cancelbtn">Cancelar</button>
                                    <button type="submit" style="padding:10px; margin:5px; width: 40%; float:rigth">
                                        Aceptar </button>
                                </div>
                            </div>
                            <!--Fin de formulario-->
                        </form>
                    </div>
                </div>




            </div>

            <div class="especificaciones">
                <!-- Muestra la informacion personal-->
                <ul>
                    <!-- Centrar en la pagina-->
                    <li><b>Nombre: <?php  echo($user['Nombre'])?> </b></li>
                    <li><b>Apellidos: <?php  echo($user['Primer Apellido']." ".$user['Segundo Apellido'])?></b></li>
                    <li><b>Correo electronico: <?php  echo($user['E-mail'])?></b> </li>
                    <li><b>Metodo de pago</b>
                        <ul class="lista">
                            <?php if($user['metodo'] = "predeterminado"){ ?>
                            <li><?php  echo($user['metodo'])?></li>
                            <?php }else{

                            }?>
                        </ul>
                    <li><b>Saldo actual: </b><?php echo($user['saldo'])?> €</li>
                    <li><b>Direccion: </b><?php echo($user['Direccion'])?></li>
                    <li><b>DNI: </b><?php if($user['dni'] != null){echo($user['dni']);} ?> </li>
                </ul>
            </div>
        </div>


        <div class="bottom">
            <div class="footer-nav">
                <div class="row">
                    <ul>
                        <li>Contacto</li>
                        <li><a href="#">Teléfono</a> </li>
                        <li><a href="#">Email</a> </li>
                        <li><a href="#">Fax</a> </li>

                    </ul>
                </div>

                <div class="row">
                    <ul>
                        <li>Privacidad</li>
                        <li><a href="#">Politica de privacidad</a> </li>
                        <li><a href="#">Uso de cookies</a> </li>
                    </ul>
                </div>

                <div class="row">
                    <ul>
                        <li>Tiendas Fisicas</li>
                        <li><a href="#">Valladolid</a></li>
                        <li><a href="#">Palencia</a></li>
                        <li><a href="#">Medina del Campo</a></li>
                        <li><a href="#">Soria</a></li>
                    </ul>
                </div>

                <div class="row">
                    <ul>
                        <li>Empresa</li>
                        <li><a href="#">Acerca de TecnoComponentes </a> </li>
                        <li><a href="#">Comunidad</a> </li>
                        <li><a href="#">Empleo</a> </li>
                        <li><a href="#">Prensa</a> </li>

                    </ul>
                </div>
            </div>


        </div>
    </div>



    <script type="text/javascript" src="Code/JS/tarjeta.js"></script>
</body>

</html>

<?php
    }
    else if( $_SERVER['REQUEST_METHOD']=='POST') {
        print_r($_POST);
        //exit();
        $u = usuarios_edit($user['username'], $_POST);
        if( $u ) {
            header('Location: perfiluser.php');
            exit;
        }
        
        else {

            echo "Error: Falló la operación";
        }
    }
?>