            function init_map() {
                var myOptions = {
                    zoom: 6,
                    center: new google.maps.LatLng(41.941038540353155, -4.508484864999307),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                /*Marcador 1*/
                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(41.941038540353155, -4.508484864999307)
                });
                infowindow = new google.maps.InfoWindow({
                    content: '<strong>Tienda Fisica</strong><br><br>34002 Palencia<br>'
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
                
                /*Marcador 2*/
                markerA = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(41.6337794, -4.7376481)
                });
                infowindowA = new google.maps.InfoWindow({
                    content: '<strong>Tienda Fisicia</strong><br><br>47006 Valladolid<br>'
                });
                google.maps.event.addListener(markerA, 'click', function() {
                    infowindowA.open(map, markerA);
                });
                i
                /*Marcador 3*/
                markerB = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(41.3028857, -4.9218578)
                });
                infowindowB = new google.maps.InfoWindow({
                    content: '<strong>Tienda Fisicia</strong><br><br>47400 Medina del Campo<br>'
                });
                google.maps.event.addListener(markerB, 'click', function() {
                    infowindowB.open(map, markerB);
                });
                
                /*Marcador 4*/
                markerC = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(41.7607811,-2.4747162,17)
                });
                infowindowC = new google.maps.InfoWindow({
                    content: '<strong>Tienda Fisica</strong><br><br>47400 Soria<br>'
                });
                google.maps.event.addListener(markerC, 'click', function() {
                    infowindowC.open(map, markerC);
                });
                
            }
            google.maps.event.addDomListener(window, 'load', init_map);