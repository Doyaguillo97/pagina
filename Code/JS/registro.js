var currentTab = 0;//hacemos que empiece en 0 el contador de vetanas
showTab(currentTab); //y mostramos la ventana en la que estamos

function showTab(n) {
  //esta muestra la centana en la que estemos 
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // y añade/modifica los botones siguiente y anterior
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Terminar";
  } else {
   
    document.getElementById("nextBtn").innerHTML = "Siguiente";
  }
//se cambia el color de los puntos de paso
  fixStepIndicator(n)
}

function nextPrev(n) {

  var x = document.getElementsByClassName("tab");
  //No pasa a la siguiente si algun campo no pasa la validacion
  if (n == 1 && !validateForm()) return false;

  x[currentTab].style.display = "none";
  //cuando pasamos de tabla aumentamos el tamaño de currenTab 
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //enviamos el formulario al terminar
    document.getElementById("regForm").submit();
    return false;
  }

  showTab(currentTab);
}

function validateForm() {
  // Comprueba si hay algun campo vacio
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // comprobamos cada campo 
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      //añadimos la clase 'invalid' para cuando falle
      y[i].className += " invalid";
        //y decimos que es falso y que no pasa la validacion
      valid = false;
    }
  }
  // cuando se pasa el bucle con todos los campos a true 
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; 
}

function fixStepIndicator(n) {
  //Quita de el estado activo de los puntos de abajo
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //pone el punto que n donde es la ventana activo
  x[n].className += " active";
}