<?php
    require_once 'back/funcionesEdit.php';
    require_once 'back/login.php';
    if( $_SERVER['REQUEST_METHOD']=='GET') {
        session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <title> TecnoComponentes S.L </title>
    <link rel="shortcut icon" type="image/x-icon" href="imagenes/favicon.ico">
    <link rel="stylesheet" type="text/css" href="Code/CSS/Estilo.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/Log-car.css">
    <link rel="stylesheet" type="text/css" href="Code/CSS/tiendasFisicas.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<body>
    <div class="header">
        <div class="logo">
            <a href="index.php"> <img src="imagenes/logo.png" href="login" height="66px"></a>
        </div>

        <div class="top-menu">
            <nav>
                <ul class="menu-bar">
                    <li><a href="index.php"> Catálogo </a></li>
                    <li> <a href="quienessomos.php"> Quienes somos </a></li>
                    <li> <a href="filosofia.php"> Filosofia comercial </a></li>
                    <li> <a href="#"> Tiendas Físicas </a></li>
                </ul>
            </nav>
        </div>

        <div class="log-car">
        <?php
            if(!isset($_SESSION['app1263467367346_islogged'])){
                print("<button onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;\">Login</button>");
                
            }elseif($_SESSION['app1263467367346_islogged']){
                print "<button onclick=\"location.href='/back/logout.php'\" style=\"width:auto;\">Logout</button>";
            }else{
                print("<button onclick=\"document.getElementById('id01').style.display='block'\" style=\"width:auto;\">Login</button>");
            }    
        ?>

            <div id="id01" class="modal">

            <form method="POST" class="modal-content animate">
                    <div class="imgcontainer">
                        <span onclick="document.getElementById('id01').style.display='none'" class="close"
                            title="Close Modal">&times;</span>
                        <img src="imagenes/avatar.png" alt="aqui" class="avatar">
                    </div>


                    <div class="container">
                        
                        
                            <label for="uname"><b>Usuario</b></label>
                            <input type="text" placeholder="Nombre de usuario" name="username" required>
                            <!--Nombre de usuario -->

                            <label for="psw"><b>Contraseña</b></label>
                            <input type="password" placeholder="•••••••••••" name="password" required>
                            <!--Contraseña mandar hasheada -->

                            <button type="submit">Login</button>
                        
                    </div>
                    
                    <!--Fin de login-->
                    <div class="container" style="background-color:#f1f1f1">
                        <button type="button" onclick="document.getElementById('id01').style.display='none'"
                            class="cancelbtn">Cancelar</button>
                        <span class="psw">¿No esta<a href="registro.php"> registrado</a>?</span>
                    </div>
                </form>
            </div>

            <button onclick="location.href='carrito.php'"class=carrito style="width:auto;">Carrito</button>

        </div>
        <div class="sombreado">

        </div>
    </div>

    <div class="cuerpo">
        <div class="caja-mapa">
            <div class="mapa">
                <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCmPF5RrjQHTwHT6w5G0e3_3tCtg6Yp4-g'></script>
                <div style='overflow:hidden;height:630px;width:489px;'>
                    <div id='gmap_canvas' style='height:630px;width:489px;'></div>

                </div> <a href='http://maps-generator.com/es'>Maps-Generator</a>
                <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=83242723f7938214f36143a436b8451f357ce63c'></script>
                <script type='text/javascript' src="Code/JS/mapa.js"></script>
            </div>
            <div>
                <h1>Tiendas Fisicas</h1>
                <p>En TecnoComponentes disponemos de 4 Tiendas Fisicas repartidas en Castilla y Leon, concretamente en Valladolid, Palencia, Medina del Campo y Soria, en todas ellas encontraras una amplia gama de los productos que vendemos en la web asi como un Servicio Tecnico disponible 12h, 6 dias a la semana. Tambien disponemos de un numero de urgencias para averias en los dispositivos de gran importancia</p>
                <p>Disponemos de varios empleados muy bien formados que estaran a su disposicion en cualquier momento que visiten la tienda.</p>

            </div>
        </div>
    </div>

    <div class="bottom">
        <div class="footer-nav">
            <div class="row">
                <ul>
                    <li>Contacto</li>
                    <li><a href="#">Teléfono</a> </li>
                    <li><a href="#">Email</a> </li>
                    <li><a href="#">Fax</a> </li>

                </ul>
            </div>

            <div class="row">
                <ul>
                    <li>Privacidad</li>
                    <li><a href="#">Politica de privacidad</a> </li>
                    <li><a href="#">Uso de cookies</a> </li>
                </ul>
            </div>

            <div class="row">
                <ul>
                    <li>Tiendas Fisicas</li>
                    <li><a href="#">Valladolid</a></li>
                    <li><a href="#">Palencia</a></li>
                    <li><a href="#">Medina del Campo</a></li>
                    <li><a href="#">Soria</a></li>
                </ul>
            </div>

            <div class="row">
                <ul>
                    <li>Empresa</li>
                    <li><a href="#">Acerca de TecnoComponentes </a> </li>
                    <li><a href="#">Comunidad</a> </li>
                    <li><a href="#">Empleo</a> </li>
                    <li><a href="#">Prensa</a> </li>

                </ul>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="Code/JS/login.js"></script>

</body>

</html>

<?php
                        }
                        else if( $_SERVER['REQUEST_METHOD']=='POST') {
                            
                            $result = login_check($_POST['username'], $_POST['password']);
                            //print_r($_POST); exit;
                            if( $result ) {
                                // Creo una sesion nueva
                                session_start();
                                $_SESSION['app1263467367346_islogged'] = true;
                                $_SESSION['username'] = $_POST['username'];
                                $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                                $_SESSION['type'] = login_privileges($result,$_POST['username']);
                                
                                //print_r($_SESSION); exit;
                                $perfil = "Location: perfil".$_SESSION['type'].".php";
                                header($perfil);//redireccionar a la pagina del usuario
                                exit;
                            }
                            else {
                                header('Location: error.php');// redireccionar a pagina de error
                                exit;
                            }
                        }
?>